#include "rpsmodel.h"

RPSModel::RPSModel(QObject *parent) : QObject(parent)
{
QTime time = QTime::currentTime();
qsrand((uint)time.msec());

yScore = 0;
oScore = 0;
}

void RPSModel::setOpponentState()
{
value = RPSEnum(rand() % 3);
qDebug() << rand() % 3;
emit opponentState(value);
}

void RPSModel::compareThings(RPSEnum choice)
{
    if(value == choice)
        emit draw();
    else if(value == PAPER && choice == SCISSORS)
    {
        yScore++;
        if(yScore == 3)
        emit endOfGame(true);
        else
        emit point4U(yScore);
    }
    else if(value == ROCK && choice == PAPER)
    {
        yScore++;
        if(yScore == 3)
        emit endOfGame(true);
        else
        emit point4U(yScore);
    }
    else if(value == SCISSORS && choice == ROCK)
    {
        yScore++;
        if(yScore == 3)
        emit endOfGame(true);
        else
        emit point4U(yScore);
    }
    else
    {
        oScore++;
        if(oScore == 3)
        emit endOfGame(false);
        else
        emit point4O(oScore);
    }
}

void RPSModel::getSelected(RPSEnum selected)
{
    setOpponentState();
    compareThings(selected);
}
