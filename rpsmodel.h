#ifndef RPSMODEL_H
#define RPSMODEL_H

#include <QtCore/QObject>
#include "rpsenum.h"

#include <QtCore/QTime>
#include <QtCore/qglobal.h>

#include <QtCore/QDebug>

class RPSModel : public QObject
{
    Q_OBJECT

    RPSEnum value;

    int yScore;
    int oScore;

public:
    explicit RPSModel(QObject *parent = 0);

private:
    void setOpponentState();
    void compareThings(RPSEnum choice);
signals:
    void point4U(int);
    void point4O(int);
    void draw();
    void endOfGame(bool);
    void opponentState(RPSEnum);
public slots:
    void getSelected(RPSEnum selected);
};

#endif // RPSMODEL_H
