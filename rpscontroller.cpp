#include "rpscontroller.h"

void RPSController::connectModelAndView()
{
    connect(model,SIGNAL(opponentState(RPSEnum)),view,SLOT(opponentSelection(RPSEnum)));
    connect(model,SIGNAL(point4O(int)),view,SLOT(point4O(int)));
    connect(model,SIGNAL(point4U(int)),view,SLOT(point4U(int)));
    connect(model,SIGNAL(endOfGame(bool)),this,SLOT(rEndOfGame(bool)));
    connect(model,SIGNAL(draw()),view,SLOT(draw()));
    connect(view,SIGNAL(selectedImage(RPSEnum)),model,SLOT(getSelected(RPSEnum)));
    connect(this,SIGNAL(sEndOfGame(bool)),view,SLOT(endOfGame(bool)));
    connect(view,SIGNAL(restart()),this,SLOT(restart()));
}

RPSController::RPSController(QObject *parent) : QObject(parent)
{
    model = new RPSModel();
    view = new RPSView();
    view->show();
    connectModelAndView();
}

RPSController::~RPSController()
{
    delete model;
    view->deleteLater();
}

void RPSController::rEndOfGame(bool state)
{
    emit sEndOfGame(state);

    disconnect(model,SIGNAL(opponentState(RPSEnum)),view,SLOT(opponentSelection(RPSEnum)));
    disconnect(model,SIGNAL(point4O(int)),view,SLOT(point4O(int)));
    disconnect(model,SIGNAL(point4U(int)),view,SLOT(point4U(int)));
    disconnect(model,SIGNAL(draw()),view,SLOT(draw()));
    disconnect(view,SIGNAL(selectedImage(RPSEnum)),model,SLOT(getSelected(RPSEnum)));
    disconnect(model,SIGNAL(endOfGame(bool)),this,SLOT(rEndOfGame(bool)));
    disconnect(this,SIGNAL(sEndOfGame(bool)),view,SLOT(endOfGame(bool)));

}

void RPSController::restart()
{
view->deleteLater();
delete model;
model = new RPSModel();
view = new RPSView();
view->show();
connectModelAndView();

}
