#include "rpsview.h"

void RPSView::initGUI()
{

    qDebug() << "Created";

   images = new QPixmap [8];
   images[0].load("rock.jpg");
   images[1].load("paper.jpg");
   images[2].load("scissors.png");

   images[3].load("point.jpg");
   images[4].load("equal.png");
   images[5].load("point4o.png");
   images[6].load("fail.jpg");
   images[7].load("win.jpg");

equalInfo = new QLabel("READY?");
equalInfo->setAlignment(Qt::AlignCenter);

wins = new QLabel("0");
lost = new QLabel("0");

wins->setAlignment(Qt::AlignCenter);
lost->setAlignment(Qt::AlignCenter);

info = new QLabel();
info->setPixmap(images[4]);

enemyGraph = new QLabel();

enemyGraph->setPixmap(QPixmap("question.jpg"));

enemyGraph->setAlignment(Qt::AlignCenter);

yourGraph = new QLabel();

yourGraph->setPixmap(QPixmap("question.jpg"));

yourGraph->setAlignment(Qt::AlignCenter);

rock = new QPushButton ();
paper = new QPushButton ();
scissors = new QPushButton();

QIcon rockIcon(images[0]);
QIcon paperIcon(images[1]);
QIcon scissorsIcon(images[2]);

rock->setIcon(rockIcon);
paper->setIcon(paperIcon);
scissors->setIcon(scissorsIcon);

rock->setIconSize(images[0].rect().size());
paper->setIconSize(images[1].rect().size());
scissors->setIconSize(images[2].rect().size());

firstH = new QHBoxLayout();
secondH = new QHBoxLayout();
thirdH = new QHBoxLayout();

firstV = new QVBoxLayout();


firstH->addWidget(yourGraph);
firstH->addWidget(equalInfo);
firstH->addWidget(enemyGraph);

secondH->addWidget(wins);
secondH->addWidget(info);
secondH->addWidget(lost);

thirdH->addWidget(rock);
thirdH->addWidget(paper);
thirdH->addWidget(scissors);

firstV->addLayout(firstH);
firstV->addLayout(secondH);
firstV->addLayout(thirdH);


mainW = new QWidget();
mainW->setLayout(firstV);
this->setCentralWidget(mainW);

QAction *restart = new QAction("&Restart", this);
QMenu *game;
game = menuBar()->addMenu("&Game");
game->addAction(restart);
connect(restart,SIGNAL(triggered(bool)),this,SLOT(restartSlot()));


connect(rock,SIGNAL(pressed()),this,SLOT(rockSelected()));
connect(paper,SIGNAL(pressed()),this,SLOT(paperSelected()));
connect(scissors,SIGNAL(pressed()),this,SLOT(scissorsSelected()));

}

RPSView::RPSView(QWidget *parent) : QMainWindow(parent)
{
    this->initGUI();
}

RPSView::~RPSView()
{
    qDebug() << "Deleted!";
    delete lost;
    delete wins;
    delete enemyGraph;
    delete yourGraph;
    delete equalInfo;
    delete rock;
    delete paper;
    delete scissors;
    delete info;
    delete [] images;
    delete mainW;
}

void RPSView::point4U(int num)
{
wins->setText(QString::number(num));
info->setPixmap(images[3]);
equalInfo->setText(">\n\nPOINT 4 YOU!");
}

void RPSView::point4O(int num)
{
lost->setText(QString::number(num));
info->setPixmap(images[5]);
equalInfo->setText("<\n\nPOINT 4 OPPONENT!");
}

void RPSView::draw()
{
info->setPixmap(images[4]);
equalInfo->setText("=\n\nEQUAL!");
}

void RPSView::endOfGame(bool areYouAWinner)
{
if(areYouAWinner)
{
wins->setText(QString::number(3));
equalInfo->setText("You\nAre\nA\nWinner!");
info->setPixmap(images[7]);
}
else
{
equalInfo->setText("Haha\nYou\nLose!");
lost->setText(QString::number(3));
info->setPixmap(images[6]);
}

}

void RPSView::opponentSelection(RPSEnum state)
{
    enemyGraph->setPixmap(images[state]);
}

void RPSView::rockSelected()
{
    yourGraph->setPixmap(images[ROCK]);
emit selectedImage(ROCK);
}

void RPSView::paperSelected()
{
    yourGraph->setPixmap(images[PAPER]);
emit selectedImage(PAPER);
}

void RPSView::scissorsSelected()
{
    yourGraph->setPixmap(images[SCISSORS]);
    emit selectedImage(SCISSORS);
}

void RPSView::restartSlot()
{
    qDebug() << "restart";
 emit restart();
}
