# README #

This is a simple Rock Paper Scissors game.

You can compile it with cmake, or add sources to new QT/VS project.

You need to add graphics in exe file folder:

-rock.jpg

-paper.jpg

-scissors.png

-point.jpg

-equal.png

-point4o.png

-fail.jpg

-win.jpg

You can compile it with CMake commands:

!*path to cmake.exe*!

/c/CMake/bin/cmake.exe -G "Visual Studio 14 2015 Win64" -DCMAKE_PREFIX_PATH=$QTDIR ..


Where QTDIR is a environment variable to QT instalation.

Eg: C:\Qt\5.6\msvc2015_64

Then just run:

!*path to cmake.exe*!

/c/CMake/bin/cmake.exe --build . --target rps --config Release

********* for linux users:

mkdir build

cd build

export QTDIR=/path_to_Qt_installation

cmake .. -DCMAKE_PREFIX_PATH=$QTDIR -DCMAKE_CXX_FLAGS '-fPIC'

make