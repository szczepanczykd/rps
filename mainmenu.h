#ifndef MAINMENU_H
#define MAINMENU_H


#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtCore/QDebug>
#include <QtCore/QObject>
#include "rpscontroller.h"

class MainMenu : public QMainWindow
{
    Q_OBJECT

    QPushButton *start;
    QPushButton *quit;
    RPSController *contro;
public:
    MainMenu(QWidget *parent = 0);
    ~MainMenu();


public slots:
    void START_GAME();
    void QUIT();


};

#endif // MAINMENU_H
