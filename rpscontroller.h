#ifndef RPSCONTROLLER_H
#define RPSCONTROLLER_H

#include <QtCore/QObject>

#include "rpsmodel.h"
#include "rpsview.h"

#include <QtCore/QDebug>

class RPSController : public QObject
{
    Q_OBJECT

    void connectModelAndView();

    RPSModel *model;
    RPSView *view;
public:
    explicit RPSController(QObject *parent = 0);
    ~RPSController();
signals:
    void sEndOfGame(bool);
public slots:
    void rEndOfGame(bool state);
    void restart();
};

#endif // RPSCONTROLLER_H
