#ifndef RPSVIEW_H
#define RPSVIEW_H

#include <QtCore/QObject>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtGui/QPixmap>
#include "rpsenum.h"
#include <QtWidgets/QPushButton>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMenu>

#include <QtCore/QDebug>
class RPSView : public QMainWindow
{
    Q_OBJECT
    void initGUI();

    QLabel *wins;
    QLabel *lost;
    QLabel *info;
    QLabel *equalInfo;

    QPushButton *rock;
    QPushButton *paper;
    QPushButton *scissors;

    QPixmap *images;
    QLabel *enemyGraph;
    QLabel *yourGraph;

    QWidget *mainW;
    QHBoxLayout *firstH;
    QHBoxLayout *secondH;
    QHBoxLayout *thirdH;

    QVBoxLayout *firstV;

public:
    explicit RPSView(QWidget *parent = 0);
    ~RPSView();
signals:
    void selectedImage(RPSEnum);
    void restart();
public slots:
    void point4U(int num);
    void point4O(int num);
    void draw();
    void endOfGame(bool areYouAWinner);
    void opponentSelection(RPSEnum state);
private slots:
    void rockSelected();
    void paperSelected();
    void scissorsSelected();
    void restartSlot();
};

#endif // RPSVIEW_H
