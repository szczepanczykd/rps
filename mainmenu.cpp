#include "mainmenu.h"

MainMenu::MainMenu(QWidget *parent)
    : QMainWindow(parent)
{

    contro = 0;
    start = new QPushButton("Start Game",this);
    quit = new QPushButton ("Quit",this);

    QVBoxLayout *layout = new QVBoxLayout();
    QWidget *main = new QWidget();

    layout->addWidget(start);
    layout->addWidget(quit);

    main->setLayout(layout);

    this->setCentralWidget(main);

    connect(start,SIGNAL(pressed()),this,SLOT(START_GAME()));
    connect(quit,SIGNAL(pressed()),this,SLOT(QUIT()));

}

MainMenu::~MainMenu()
{
delete start;
delete quit;
delete contro;
}

void MainMenu::START_GAME()
{

    if(contro)
    delete contro;
    contro = new RPSController();
    this->hide();

}

void MainMenu::QUIT()
{
    this->close();
}

